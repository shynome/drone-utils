const shell = require('shelljs')

module.exports = class Git {

  /**
   * @param {string} commit 
   * @param {string} file 
   */
  static show(commit,file){
    return shell.exec(`git show ${commit}:${file}`,{ silent: true }).toString()
  }
}