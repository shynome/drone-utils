const [ prefix='' ] = process.argv.slice(2)

/**@param {string} npm_package_version  */
function get_tags(npm_package_version){

  const [ version, suffix='' ] = npm_package_version.split('-')
  
  let tags = version.split('.').map((v,i,a)=>a.slice(0,i+1).join('.')+(suffix && '-')+suffix)

  let flag = 'latest'

  switch(true){
    case tags[0][0]==='0':
      flag='dev'
      break;
    case suffix!=='':
      flag='beta'
      break;
  }
  
  if(suffix!==''){
    tags = tags.slice(-1)
  }
  
  tags.push(flag)

  if(prefix){
    tags = tags.map(tag=>{
      if(tag==='latest'){
        tag = ''
        return prefix
      }
      return [prefix,tag].join('-')
    })
  }

  return tags.join(',')
  
}

const branch = process.env['DRONE_BRANCH']
const event = /**@type { 'push' | 'pull_request' | 'tag' } */(process.env['DRONE_BUILD_EVENT'])

let tags = ''

if(event==='tag'){
  // @ts-ignore
  tags = get_tags(require('./package').version)
}else if(branch){
  tags = [prefix,branch].filter(v=>v).join('-')
}

if(tags){
  const fs = require('fs'), path = './.tags'
  fs.writeFileSync(path,tags)
  console.log(`tags: ${fs.readFileSync(path,'utf8')}`)
}else{
  console.log('no tags can set')
}
