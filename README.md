## npm scripts

### set_tags 

在 `.drone.yml` 中设置 tags

```sh
# 推荐这种, 不会像下一个有变量污染
node -e "s='';require('https').get('https://gitlab.com/shynome/drone-utils/raw/master/set_tags.js',r=>r.on('data',d=>s+=d).on('end',()=>console.log(s)))" | node
node -e "s='';require('https').get('https://gitlab.com/shynome/drone-utils/raw/master/set_tags.js',r=>r.on('data',d=>s+=d).on('end',()=>eval(s)))"
wget -q -O - https://gitlab.com/shynome/drone-utils/raw/master/set_tags.js | node
curl https://gitlab.com/shynome/drone-utils/raw/master/set_tags.js | node
```